﻿using DBRepository;
using EncryptionServices.Hashing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakeDB
{
    public class FakeClientRepository
    {
        List<ClientMetadata> _data = new List<ClientMetadata>();
        public FakeClientRepository()
        {
            IHashingModule<string> shPass = new SHAHashingModule<string>();
            
            
        }

        public static void FillClientDummyData(IContext<ClientMetadata> _data)
        {
            IHashingModule<string> shPass = new SHAHashingModule<string>();
            _data.Add(new ClientMetadata()
            {
                Email = "akshita@mail.com",
                FirstName = "Akshita",
                HashKey = shPass.ComputeHash("akshita@12345"),
                ID = 1,
                Image = "",
                IsActive = true,
                LastName = "Srivastav",
                MiddleName = ""

            });
            _data.Add(new ClientMetadata()
            {
                Email = "florence@mail.com",
                FirstName = "Florence",
                HashKey = shPass.ComputeHash("florence@12345"),
                ID = 2,
                Image = "",
                IsActive = true,
                LastName = "D'souza",
                MiddleName = ""

            });
        }
    }
}
