﻿using DBRepository;
using PinkFlute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakeDB
{
    public class FakeAuthenticationService : BaseAuthenticationService
    {
        IContext<ClientMetadata> _repo = null;
        public FakeAuthenticationService(IContext<ClientMetadata> _repo)
            : base(_repo)
        {
            _repo = InstanceService.GetInstance<IContext<ClientMetadata>>();
        }
        public override AuthenticateStatus Authenticate(ClientCredentials credentials)
        {
            try
            {
                var _userdata = _repo.Get(p => p.Email == credentials.Email && p.HashKey == credentials.HashKey).SingleOrDefault();
                if (_userdata == null || _userdata == default(ClientMetadata))
                {
                    return AuthenticateStatus.Failure;
                }
                if (!_userdata.IsActive)
                {
                    return AuthenticateStatus.InActive;
                }
                return AuthenticateStatus.Success;
            }
            catch (Exception ex)
            {

                return AuthenticateStatus.UnKnown;
            }
        }

        public override ClientMetadata GetMetadata(int ID)
        {
            try
            {
                return _repo.GetByID(ID);
            }
            catch (Exception ex)
            {

                return null;
            }
        }
    }
}
