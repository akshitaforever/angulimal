﻿using DBRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FakeDB
{
    public class FakeContext<T>:IContext<T> where T : BaseEntity
    {
        List<T> _data=new List<T>();
        public FakeContext(string connectionstring)
        {
           

        }
        public FakeContext()
          
        {
           

        }
       

        public T Add(T item)
        {

          this._data.Add(item);
            return item;
        }

        public T Update(T item)
        {
           var obj=_data.Find(p=>p.ID==item.ID);
            _data[_data.IndexOf(obj)]=item;
            return item;
        }

        public bool Delete(T item)
        {
            var obj=_data.Find(p=>p.ID==item.ID);
            _data.Remove(obj);
            return true;
        }

        public T GetByID(int ID)
        {
            return this._data.Single(p=>p.ID==ID);
        }




        public IEnumerable<T> Get(Expression<Func<T, bool>> expression, Expression<Func<T, string>> orderby = null)
        {
            var query = this._data.Where(expression.Compile());
            if (orderby != null)
            {
                query = query.OrderBy(orderby.Compile());
            }
            return query.AsEnumerable();
        }

        public IEnumerable<T> GetPaged(Expression<Func<T, string>> orderby, Expression<Func<T, bool>> expression = null, int page = 1, int itemcount = 10)
        {
            var query = this._data.AsEnumerable();
            if (expression != null)
            {
                query = query.Where(expression.Compile());
            }
            Type entityType = typeof(T);
            var items = query.OrderBy(orderby.Compile()).Skip(page * itemcount).Take(itemcount).AsEnumerable().ToList();
         
            return items;

        }

        public T GetSingle(Expression<Func<T, bool>> expression)
        {
            return this._data.Single(expression.Compile());
        }

        public bool CreateDB(string connstring)
        {
            try
            {
               
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
 
}
