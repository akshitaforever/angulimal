﻿using DBRepository;
using EncryptionServices;
using EncryptionServices.Hashing;
using NetworkService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AngulimalClient
{
    public partial class Form1 : Form
    {
        private IPEndPoint endpoint = new System.Net.IPEndPoint(IPAddress.Parse("10.97.87.74"), 1008);
        IHashingModule<string> shPass = new SHAHashingModule<string>();
        public Form1()
        {
            InitializeComponent();
        }
        public void ConnectToServer()
        {
            NetworkTcpClient client = new NetworkTcpClient(endpoint);
            client.Connect(endpoint.Address, 1008);
            if (client.Connected)
            {
                ClientCredentials mdata = new ClientCredentials() { Email = "akshita@mail.com", HashKey = shPass.ComputeHash("akshita@12345") };
                NetworkOperation.TransferMessage<ClientCredentials>(mdata, client);
            }

          


        }
     
    }
}
