﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigurationServices
{
    public class AppConfigConfigurationSource : IConfigurationSource
    {
        public void Initialize()
        {

        }

        public bool IsInitialized()
        {
            return true;
        }

        public string GetValue(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public Dictionary<string, string> GetAllSettings()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            ConfigurationManager.AppSettings.AllKeys.ToList().ForEach(a => dict.Add(a, ConfigurationManager.AppSettings[a]));
            return dict;
        }

        public bool ContainsKey(string Key)
        {
            return ConfigurationManager.AppSettings.AllKeys.Contains(Key);
        }


      
    }
}
