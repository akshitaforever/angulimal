﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigurationServices
{
    public interface IConfigurationSource
    {
        void Initialize();
        bool IsInitialized();
        string GetValue(string key);
        Dictionary<string, string> GetAllSettings();
        bool ContainsKey(string Key);
    }
}
