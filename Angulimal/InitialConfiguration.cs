﻿using ConfigurationServices;
using DBRepository;
using PinkFlute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angulimal
{
    public class InitialConfiguration
    {
        public static void CreateNew()
        {
            Container container = new Container();
            //TEST MODE ONLY
            FakeDB.FakeContext<ClientMetadata> _repo = new FakeDB.FakeContext<ClientMetadata>();
            FakeDB.FakeClientRepository.FillClientDummyData(_repo);
            //
            container.RegisterStatic<IConfigurationSource, AppConfigConfigurationSource>(new AppConfigConfigurationSource());
            container.RegisterStatic<IContext<ClientMetadata>, FakeDB.FakeContext<ClientMetadata>>(_repo);
            container.Register<BaseAuthenticationService, FakeDB.FakeAuthenticationService>().WithParameters<IContext<ClientMetadata>>(_repo);
            InstanceService._diinstance = container;
            var obj = container.GetInstance<BaseAuthenticationService>();
        }
    }

}
