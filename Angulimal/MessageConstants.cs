﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Angulimal
{
    public class MessageConstants
    {
        public static string CONNECTED_MESSAGE
        {
            get
            {
                return "Angulimal Server by Akshita Srivastav Version " + Application.ProductVersion + " Welcome " + DateTime.Now.ToString() + "IST *OK";
            }
        }

    }
}
