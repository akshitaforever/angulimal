﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using NetworkService;
using System.Threading;
using DBRepository;
using EncryptionServices;
namespace Angulimal
{
    public partial class AngulimalServer : Form
    {
        private IPAddress localaddress = null;
        IContext<ClientMetadata> _repo = null;
        Dictionary<ClientMetadata, ClientState> _clients = new Dictionary<ClientMetadata, ClientState>();

        Thread serverthread = null;

        private bool invalid = false;
        public AngulimalServer()
        {
            InitializeComponent();
            Initialize();
            GetLocalIP();

        }

        private void Initialize()
        {


        }
        public void GetLocalIP()
        {
            var ips = Dns.GetHostAddresses("");
            invalid = !ips.Any();
            btn_start.Enabled = !invalid;
            cmb_IP.DataSource = ips.Select(s => new ExtendedIPAddress(s)).ToList();



        }
        public void InitConnection()
        {
            NetworkTcpListener listener = new NetworkTcpListener(localaddress, 1008);
            listener.Start();
            while (true)
            {
                var iresult = listener.BeginAcceptTcpClient(ClientAccepted, listener);


            }
        }

        private void ClientAccepted(IAsyncResult ar)
        {
            var listener = ar.AsyncState as NetworkTcpListener;
            var _client = listener.EndAcceptTcpClient(ar);
            SendMessage(_client, MessageConstants.CONNECTED_MESSAGE);

        }
        private void GetHeaderFromClient(NetworkTcpClient client)
        {
            try
            {
                NetworkObjectStream strm = client.GetStream();
                var data = strm.ReadObject<ClientCredentials>();

            }
            catch (Exception ex)
            {

            }




        }
        private string ProcessRequest(string requeststring)
        {
            string response = "";
            switch (requeststring)
            {
                case "PING":
                    response = MessageConstants.CONNECTED_MESSAGE;
                    break;
                default:
                    break;
            }
            return response;
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            if (!invalid)
            {
                localaddress = (cmb_IP.SelectedValue as ExtendedIPAddress).IPAddress;
                serverthread = new Thread(() => InitConnection());
                serverthread.IsBackground = true;
                serverthread.Start();
            }

        }
        private void SendMessage(NetworkTcpClient client, string msg)
        {
            NetworkStream strm = client.GetStream();
            byte[] data = Encoding.UTF8.GetBytes(msg);
            strm.Write(data, 0, data.Length);
        }
    }
}
