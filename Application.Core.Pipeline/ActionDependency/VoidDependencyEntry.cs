﻿using Pipeline.ActionDependency.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline.ActionDependency
{
    public class VoidDependencyEntry:IDependencyEntry
    {
        public Action DependencyAction { get; set; }

        public void Execute()
        {
             DependencyAction.Invoke();
        }

        public IAsyncResult ExecuteAsync(AsyncCallback callback, object state)
        {
            return DependencyAction.BeginInvoke(callback, state);
        }
        public System.Reflection.MethodInfo GetMethod()
        {
            return DependencyAction.Method;
        }
    }
}
