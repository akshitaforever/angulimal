﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteDB.Initializers
{
    public class SQLIteHelper
    {
        private readonly string _path = "";
        public SQLIteHelper(string connstring)
        {
            _path = connstring.Split('=')[1].Replace(";", "");
        }
        public bool InitializeDB()
        {
            try
            {
                SQLiteConnection.CreateFile(_path);

                using (var m_dbConnection = new SQLiteConnection(string.Format("Data Source={0};Version=3;", _path)))
                {
                    m_dbConnection.Open();
                    string sql = ReadTextFile(System.IO.Directory.GetCurrentDirectory() + Constants.SCRIPT_PATH);

                    SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception)
            {

                return false;
            }
        }
        private string ReadTextFile(string path)
        {
            return System.IO.File.ReadAllText(path);
        }
    }
}
