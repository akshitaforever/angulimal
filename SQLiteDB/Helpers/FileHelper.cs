﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace SQLiteDB.Helpers
{
    public static class FileHelper
    {
        public static bool CheckSQLiteFileExists(string connstring)
        {

            string _path = connstring.Split('=')[1].Replace(";", "");
            return File.Exists(connstring);
        }

    }
}
