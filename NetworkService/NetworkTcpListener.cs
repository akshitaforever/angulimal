﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace NetworkService
{
    public class NetworkTcpListener : TcpListener
    {

        [Obsolete("This method has been deprecated. Please use NetworkTcpListener(IPAddress localaddr, int port) instead.")]
        public NetworkTcpListener(int port)
            : base(port)
        { }

        public NetworkTcpListener(IPEndPoint localEP)
            : base(localEP)
        { }
        public NetworkTcpListener(IPAddress localaddr, int port)
            : base(localaddr, port)
        { }
        public new NetworkTcpClient EndAcceptTcpClient(IAsyncResult asyncResult)
        {
            return base.EndAcceptTcpClient(asyncResult) as NetworkTcpClient;
        }

        public new NetworkTcpClient AcceptTcpClient()
        {
            return base.AcceptTcpClient() as NetworkTcpClient;
        }
      
      
      
     
      
        public IAsyncResult BeginAcceptTcpClient(AsyncCallback callback, object state)
        {
            return base.BeginAcceptTcpClient(callback, state);
        }
        public static new NetworkTcpListener Create(int port)
        {
            return new NetworkTcpListener(port);
        }
     
      
    }
}
