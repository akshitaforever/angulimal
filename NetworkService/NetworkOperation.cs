﻿using ConfigurationServices;
using EncryptionServices;
using PinkFlute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
namespace NetworkService
{
    public enum MessageStatus
    {
        Sent, Sending, Failed, UnKnown
    }
    public static class NetworkOperation
    {
        private static int retryCount = Int32.Parse(InstanceService.GetInstance<IConfigurationSource>().GetValue(ConfigurationKeys.RETRY_COUNT));
        public static MessageStatus TransferMessage<T>(T Message, NetworkTcpClient client) where T : class
        {


            while (retryCount > 0)
            {
                try
                {
                    NetworkStream strm = client.GetStream();
                    byte[] data = StreamUtilities.ObjectToByteArray<T>(Message);
                    strm.Write(data, 0, data.Length);
                    return MessageStatus.Sent;
                }
                catch (Exception)
                {
                    retryCount--;
                    return MessageStatus.Failed;
                }
            }
            return MessageStatus.UnKnown;
        }
        public static MessageStatus TransferMessage(string Message, NetworkTcpClient client)
        {
            while (retryCount > 0)
            {
                try
                {
                    NetworkStream strm = client.GetStream();
                    byte[] data = Encoding.UTF8.GetBytes(Message);
                    strm.Write(data, 0, data.Length);
                    return MessageStatus.Sent;
                }
                catch (Exception)
                {
                    retryCount--;
                    return MessageStatus.Failed;
                }
            }
            return MessageStatus.UnKnown;
        }
        public static Object GetMessage(NetworkTcpClient client)
        {
            return null;
        }
    }
}
