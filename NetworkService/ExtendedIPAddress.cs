﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NetworkService
{
    public class ExtendedIPAddress
    {
        IPAddress _ipaddress = null;
        public ExtendedIPAddress(IPAddress address)
        {
            this._ipaddress = address;
        }
        public override string ToString()
        {

            return _ipaddress.IsIPv4MappedToIPv6 ? _ipaddress.MapToIPv4().ToString() : _ipaddress.ToString();
        }
        public IPAddress IPAddress { get; set; }
    }
}
