﻿using EncryptionServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace NetworkService
{
    public class NetworkObjectStream : NetworkStream
    {

        public NetworkObjectStream(Socket socket)
            : base(socket)
        {

        }

        public NetworkObjectStream(Socket socket, bool ownsSocket)
            : base(socket, ownsSocket)
        {

        }

        public NetworkObjectStream(Socket socket, FileAccess access)
            : base(socket, access)
        {

        }


        public NetworkObjectStream(Socket socket, FileAccess access, bool ownsSocket)
            : base(socket, access, ownsSocket)
        {

        }
        public T ReadObject<T>() where T : class
        {

            List<byte> datalst = new List<byte>();
            Byte[] bytes = new Byte[256];
            int i = 0;
            int offset = 0;
            //MemoryStream ms = new MemoryStream();
            //strm.BaseStream.CopyTo(ms);

            do
            {
                i = Read(bytes, 0, bytes.Length);
                datalst.AddRange(bytes);
            }
            while (DataAvailable);
            return StreamUtilities.GetObject<T>(datalst.ToArray());
        }
        public T TryReadObject<T>() where T : class
        {

            List<byte> datalst = new List<byte>();
            Byte[] bytes = new Byte[256];
            int i = 0;
            int offset = 0;
            //MemoryStream ms = new MemoryStream();
            //strm.BaseStream.CopyTo(ms);

            do
            {
                i = Read(bytes, 0, bytes.Length);
                datalst.AddRange(bytes);
            }
            while (DataAvailable);
            var data = StreamUtilities.GetObject<object>(datalst.ToArray());
            if (data is T)
                return data as T;
            return null;
        }
        public void WriteObject<T>(T obj) where T : class
        {
            byte[] data = StreamUtilities.ObjectToByteArray<T>(obj);
            Write(data, 0, data.Length);
        }
        public void TryWriteObject<T>(T obj, out bool success) where T : class
        {
            success = false;
            try
            {
                byte[] data = StreamUtilities.ObjectToByteArray<T>(obj);
                Write(data, 0, data.Length);
            }
            catch (Exception)
            {
                success = false;

            }

        }
    }
}
