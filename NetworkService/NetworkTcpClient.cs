﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace NetworkService
{
    public class NetworkTcpClient : TcpClient
    {
        // Summary:
        //     Initializes a new instance of the System.Net.Sockets.NetworkTcpClient class.
        public NetworkTcpClient()
            : base()
        {

        }

        public NetworkTcpClient(AddressFamily family)
            : base(family)
        {

        }

        public NetworkTcpClient(IPEndPoint localEP)
            : base(localEP)
        {
            
        }

        public NetworkTcpClient(string hostname, int port)
            : base(hostname, port)
        { }
        public new NetworkObjectStream GetStream()
        {
            return base.GetStream() as NetworkObjectStream;
        }
    }
}
