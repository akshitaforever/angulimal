﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBRepository
{
    public enum AuthenticateStatus
    {
        Success, InActive, Failure, UnKnown
    }
    public abstract class BaseAuthenticationService
    {
        public BaseAuthenticationService(IContext<ClientMetadata> _repo)
        {

        }
        public abstract AuthenticateStatus Authenticate(ClientCredentials credentials);
        public abstract ClientMetadata GetMetadata(int ID);
    }

}
