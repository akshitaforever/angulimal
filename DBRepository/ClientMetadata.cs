﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBRepository
{
    public class ClientMetadata:BaseEntity
    {
       
        public string HashKey { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
        public ICollection<ClientMetadata> GroupList { get; set; }
    }

    public class ClientCredentials
    {
        public string Email { get; set; }
        public string HashKey { get; set; }
    }
}
