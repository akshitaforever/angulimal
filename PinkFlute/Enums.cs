﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinkFlute
{ 
    public class Enums
    {
        public enum InstanceMode
        {
            Singleton, Predicate, PerInstance, Static
        }
    }
}
