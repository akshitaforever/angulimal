﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PinkFlute
{
    public class TypeMeta<TService, TImpl> where TImpl : class, TService
    {
        private Type TInterface { get; set; }
        private Type TConcrete { get; set; }
        private Func<Type> predicate { get; set; }
        private ContainerEntry<TService, TImpl> _entry { get; set; }
        private Object StateObject { get; set; }
        private Object _inst { get; set; }
        public Enums.InstanceMode Mode { get; set; }
        private bool _hascustom;
        public TypeMeta(Type TInterface, Type TConcrete, Enums.InstanceMode Mode, Func<Type> predicate = null, Object StateObject = null, object instance = null)
        {
            this.TInterface = TInterface;
            this.TConcrete = TConcrete;
            this.Mode = Mode;
            this.predicate = predicate;
            this.StateObject = StateObject;
            this._entry = new ContainerEntry<TService, TImpl>();
            this._inst = instance;

        }
        public TypeMeta(TImpl obj)
        {
            _inst = obj;
        }
        public ContainerEntry<TService, TImpl> GetContainerEntry()
        {
            return this._entry;
        }
        public bool HasEntry(Type TService)
        {
            return TService == TInterface;
        }
        public TImpl GetObject<TImpl>() where TImpl : class
        {

            switch (Mode)
            {
                case Enums.InstanceMode.Singleton: return GetSingleton<TImpl>();
                    break;
                case Enums.InstanceMode.Predicate: return GetPredicateOutput<TImpl>();
                    break;
                case Enums.InstanceMode.PerInstance: return GetObjectByDefault<TImpl>();
                    break;
                case Enums.InstanceMode.Static: return GetObjectStatic<TImpl>();
                    break;
                default:
                    break;
            }
            return null;

        }
        private TImpl GetObjectWithConstructor<TImpl>() where TImpl : class
        {
            if (_entry.GetArguments().Count > 0)
                return Activator.CreateInstance(typeof(TImpl), _entry.GetArguments().ToArray()) as TImpl;
            else
            {
                var k = typeof(TImpl);
                return Activator.CreateInstance(typeof(TImpl)) as TImpl;
            }
        }
        private TImpl GetObjectStatic<TImpl>() where TImpl : class
        {
            return _inst as TImpl;
        }
        private TImpl GetSingleton<TImpl>() where TImpl : class
        {
            return _inst != null ? _inst as TImpl : GetObjectWithConstructor<TImpl>();
        }
        private TImpl GetPredicateOutput<TImpl>() where TImpl : class
        {
            if (predicate == null)
                throw new ArgumentNullException("predicate is Undefined");
            return GetExplicitObject(predicate()) as TImpl;
        }
        private TImpl GetObjectByDefault<TImpl>() where TImpl : class
        {
            return GetObjectWithConstructor<TImpl>();
        }

        public Object GetObject()
        {

            if (this._entry.GetArguments().Count > 0)
            {
                var args = _entry.GetArguments().OrderBy(p => p.Key).Select(s => s.Value).ToArray();
                var cinfo = TConcrete.GetConstructors();
                var arg = cinfo.Single(p => p.GetParameters().Count() == args.Length);


                if (arg != null)
                {
                    foreach (ParameterInfo item in arg.GetParameters())
                    {
                        if (item.ParameterType.GenericTypeArguments.Count() > 0)
                        {
                            var obj = args.Single(p => p.GetType().GetInterface(item.ParameterType.Name) != null);
                            args[Array.IndexOf(args, obj)] = item.ParameterType.MakeGenericType(obj.GetType());
                        }
                    }
                    return Activator.CreateInstance(TConcrete,
                                       args);
                }
                else
                {
                    return null;
                }

            }
            else
                return Activator.CreateInstance(TConcrete) as TImpl;
        }
        public Object GetExplicitObject(Type ObjType, params Object[] args)
        {
            if (args.Length > 0)
                return Activator.CreateInstance(TConcrete, _entry.GetArguments().ToArray());
            else
                return Activator.CreateInstance(ObjType) as TImpl;

        }

    }

}
