﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncryptionServices.Hashing
{
    public interface IHashingModule<T>
    {
        string ComputeHash(T obj);
        byte[] ComputeHashToBytes(T obj);
    }
}
