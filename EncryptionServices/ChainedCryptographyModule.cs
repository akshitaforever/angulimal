﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncryptionServices
{
    public class SingletonCryptographyModule<T> : AbstractCryptographyModule<T> where T : class
    {
       

        public override Byte[] Encrypt(T data)
        {
            Byte[] rawdata = StreamUtilities.ObjectToByteArray(data);
            for (int i = 0; i < CipherCycle; i++)
            {
                foreach (var item in _cipher.OrderBy(p => p.Key).Select(s => s.Value))
                {
                    rawdata = item.Encrypt(rawdata);
                }
            }
         
           
            return rawdata;
        }

        public override T Decrypt(byte[] data)
        {
            for (int i = 0; i < CipherCycle; i++)
            {
                foreach (var item in _cipher.OrderByDescending(p => p.Key).Select(s => s.Value))
                {
                    data = item.Encrypt(data);
                }
            }
         
            return StreamUtilities.GetObject<T>(data);
        }


        public override void SetCycle(int count)
        {
            if (count < 1)
                throw new ArgumentException("Cycle must be grater than zero");
            this.CipherCycle = count;
        }
    }
}
