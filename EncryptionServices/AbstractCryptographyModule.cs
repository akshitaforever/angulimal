﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
namespace EncryptionServices
{

    public abstract class AbstractCryptographyModule<T> where T : class
    {
        protected internal Encoding @Encoding { get; set; }
        protected internal int CipherCycle { get; set; }
        protected internal SortedList<int, AbstractEncryptionComponent<T>> _cipher = new SortedList<int, AbstractEncryptionComponent<T>>();
        public abstract Byte[] Encrypt(T data);
        public abstract T Decrypt(Byte[] data);
        public abstract void SetCycle(int count);
    }

}

